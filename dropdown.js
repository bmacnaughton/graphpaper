function DropDown(el, options) {
    this.dd = el;
    this.placeholder = this.dd.children('span');
    this.placeholderText = options.label || this.placeholder.text() || '';
    this.opts = this.dd.find('ul.dropdown > li');
    this.val = '';
    this.index = -1;

    options = options || {};
    this.callback = options.callback;

    if (options.index !== undefined) {
        this.index = options.index;
        this.val = this.opts.eq(options.index).text();
        this.placeholder.text(this.placeholderText + this.val);
    }

    this.initEvents();
}

DropDown.prototype = {
    initEvents : function() {
        var obj = this;

        obj.dd.on('click', function(event){
            event.preventDefault();
            event.stopPropagation();
            $(this).toggleClass('active');
            //return false;
        });

        obj.opts.on('click', function(){
            var opt = $(this);
            obj.val = opt.text();
            obj.index = opt.index();
            obj.placeholder.text(obj.placeholderText + obj.val);
            if (obj.callback) {
                obj.callback(obj.index, obj.val);
            }
        });
    },
    getValue : function() {
        return this.val;
    },
    getIndex : function() {
        return this.index;
    }
}
