'use strict';

function GraphPaper(range, opts) {
    this.opts = opts || {};
    this.center = new paper.Point(paper.view.size.divide(2));
    this.width = paper.view.size.width;
    this.height = paper.view.size.height;
    // divide by two for positive and negative axes or, for polar,
    // because each ring is of this radius.
    this.limitingSize = Math.min(this.width, this.height)/2;
    this.graphType = this.opts.type || "linear";
    // range is number of units on each side of short axis or,
    // for a polar graph, the number of rings. for polar we
    // add 1 to the number so there is room left for the degree
    // labels.
    this.range = range;
    if (this.graphType === "polar") {
        range += 1.5;
    }
    this.spacing = this.limitingSize/range;
    this.graphpaper = new paper.Group();
    this.graphpaper.name = "graphpaper";

    if (this.graphType === "linear") {
        this.makeLinear();
    } else if (this.graphType === "polar") {
        this.makePolar();
    } else {
        throw "Invalid GraphPaper type: " + this.graphType;
    }

    // force an update for the view
    paper.view.update();

}

GraphPaper.prototype.makeLinear = function() {
    // get the number of x and y lines
    var nx = this.width/2/this.spacing;
    var ny = this.height/2/this.spacing;

    // make the center lines
    var line = new paper.Path.Line({
        from: [0, this.center.y],
        to: [this.width, this.center.y],
        strokeColor: 'black',
        strokeWidth: 1.5
    })
    this.graphpaper.addChild(line);

    line = new paper.Path.Line({
        from: [this.center.x, 0],
        to: [this.center.x, this.height],
        strokeColor: 'black',
        strokeWidth: 1.5
    })
    this.graphpaper.addChild(line);

    // draw y-lines
    for (var i = 1; i <= ny; i++) {
        line = new paper.Path.Line({
            from: [0, this.center.y + i * this.spacing],
            to: [this.width, this.center.y + i * this.spacing],
            strokeColor: 'grey',
            strokeWidth: 1.0
        });
        this.graphpaper.addChild(line);

        line = new paper.Path.Line({
            from: [0, this.center.y + i * -this.spacing],
            to: [this.width, this.center.y + i * -this.spacing],
            strokeColor: 'grey',
            strokeWidth: 1.0
        });
        this.graphpaper.addChild(line);
    }

    // draw x-lines
    for (i = 1; i <= nx; i++) {
        line = new paper.Path.Line({
            from: [this.center.x + i * this.spacing, 0],
            to: [this.center.x + i * this.spacing, this.height],
            strokeColor: 'grey',
            strokeWidth: 1.0
        });
        this.graphpaper.addChild(line);

        line = new paper.Path.Line({
            from: [this.center.x + i * -this.spacing, 0],
            to: [this.center.x + i * -this.spacing, this.height],
            strokeColor: 'grey',
            strokeWidth: 1.0
        })
        this.graphpaper.addChild(line);
    }

}

GraphPaper.prototype.makePolar = function() {
    //var nrings = this.limitingSize / this.spacing;
    var nrings = this.range;
    var lastRadius = this.range * this.spacing;
    var circle, line;

    function addLabels() {
        var line = new paper.Path.Line({
            from: from,
            to: to,
            visible: false
        });
        var offsets, adjustments;
        for (var i = 0; i < 4; i++) {
            var degrees = i * 90;
            var point = line.segments[1].point;
            var label = new paper.PointText({
                //point: line.segments[1].point.add(this.spacing/10, 0),
                point: point,
                content: degrees,
                fillColor: 'black',
                fontFamily: "Courier New",
                fontWeight: 'bold',
                fontSize: Math.floor(this.spacing * 0.6), /* 0.45 */
            });
            offsets = label.bounds.size;
            // adjust position of label according to positioning. 180 degrees
            // needs to account for adding the degree sign. the degree sign
            // is added later so the numbers are centered on the line.
            if (degrees === 0) {
                adjustments = [this.spacing/10, offsets.height * 0.25];
            } else if (degrees === 90) {
                adjustments = [-offsets.width/2, -this.spacing/10];
            } else if (degrees === 180) {
                adjustments = [-offsets.width * 1.35, offsets.height * 0.25];
            } else {
                adjustments = [-offsets.width/2, offsets.height * 0.75];
            }
            label.content = label.content + "°";
            
            label.position = label.position.add(adjustments);
            this.graphpaper.addChild(label);
            line.rotate(-90);
        }
        line.remove();
    }

    // there is no ring in the center, so start with i = 1. Stop
    // before nrings because the last ring reserves space for the
    // labels.
    for (var i = 1; i <= nrings; i++) {
        circle = new paper.Path.Circle({
            center: this.center,
            radius: this.spacing * i,
            strokeColor: "darkgrey",
            strokeWidth: 1.0
        });
        this.graphpaper.addChild(circle);
    }

    var from = new paper.Point(this.center.x - lastRadius, this.center.y);
    var to = new paper.Point(this.center.x + lastRadius, this.center.y);

    // the limitingSize is also the distance from the center that the
    // lines need to start and end.
    line = new paper.Path.Line({
        from: from,
        to: to,
        strokeColor: "steelblue",
        strokeWidth: 1.0
    });
    this.graphpaper.addChild(line);

    addLabels.call(this);

    /*
    var label = new paper.PointText({
        point: line.segments[1].point.add(this.spacing/10, 0),
        content: "0°",
        fillColor: 'black',
        fontFamily: "Courier New",
        fontSize: Math.floor(this.spacing/2)
    });
    this.graphpaper.addChild(label);
    // */

    for (i = 1; i < 6; i++) {
        line = line.clone();
        line.rotate(30);
        this.graphpaper.addChild(line);
    }
}

GraphPaper.prototype.getAdjustFunction = function() {
    var spacing = this.spacing;
    var center = this.center;

    if (this.graphType === "linear") {
        return function(point) {
            var ap = point.multiply(spacing);
            return new paper.Point(ap.x + center.x, center.y - ap.y);
        };
    } else if (this.graphType === "polar") {
        return function(point) {
            // convert it from (r, theta) format
            var r = point.x;
            var theta = point.y * (Math.PI/180);
            var ap = new paper.Point(r * Math.cos(theta), r * Math.sin(theta));
            // then normal conversion
            ap = ap.multiply(spacing);
            return new paper.Point(ap.x + center.x, center.y - ap.y);
        }
    } else {
        throw "Invalid graphType: " + this.graphType;
    }
}

GraphPaper.prototype.polarToCartesian = function(r, theta) {
    // convert degrees to radians (reverse: degrees = radian * (180/PI))
    theta = theta * (Math.PI/180);
    
    return new paper.Point(r * Math.cos(theta), r * Math.sin(theta));
}

//
// ItemMachine
//

function ItemMachine(range, opts) {
    this.opts = opts = opts || {};
    
    GraphPaper.call(this, range, this.opts.graphOpts);
    this.adjust = this.getAdjustFunction();
    this.accel = opts.accelerator || 1.0;
    this.items = [];
}

ItemMachine.prototype = Object.create(GraphPaper.prototype);
ItemMachine.prototype.constructor = ItemMachine;

//
// this function will be called to adjust line coordinates supplied by the user.
// this allows, e.g., a [0, 0] to be centered in the view and each user point
// to be an arbitrary number of pixels.
//
/*
ItemMachine.prototype.setAdjustments = function(func) {
    this.adjust = func;
}
// */

//
// take the caller's line definition (an object argument to Path.Line) and
// return a ItemMachine object with transformed coordinates and additional
// information as well as the caller's line definition.
//
ItemMachine.prototype.getLineDef = function(line) {
    var vector;
    var accel = this.accel;
    line = $.extend({}, line);

    line.from = this.adjust(line.from);
    line.to = this.adjust(line.to);

    vector = line.to.subtract(line.from);

    // note that this in the setup and render function is the item being
    // drawn, not the ItemMachine object.
    return {
        line: line,
        item: "line",
        incr: vector.divide(vector.length).multiply(accel),
        iter: vector.length / accel,
        //
        // setup a line so it starts and ends with the same point, allowing
        // it to be rendered by changing the second segment point as opposed
        // to creating a new line and removing the old line each iteration.
        //
        setup: function() {
            var path;
            var originalTo = this.line.to;
            this.line.to = this.line.from;
            this.path = new paper.Path.Line(this.line);
            this.line.to = originalTo;
        },
        // the argument i is the iteration number for this item
        render: function (i) {
            var segp0 = this.path.segments[0].point;
            var segp1 = new paper.Point(segp0.add(this.incr.multiply(i)));
            this.path.segments[1].point = segp1;
        }
    };
}

//
// take the caller's circle definition (an object argument to Path.Circle) and
// return a ItemMachine object with transformed coordinates and additional
// information as well as the caller's original line definition.
//
ItemMachine.prototype.getCircleDef = function(circle) {
    var vector;

    circle = $.extend({}, circle);

    circle.center = this.adjust(circle.center);


    // note that this in the setup and render function is the item being
    // drawn, not the ItemMachine object.
    return {
        circle: circle,
        incr: null,
        iter: 1,
        //
        // setup for the circle does nothing other than create the path.
        //
        setup: function() {
            this.path = new paper.Path.Circle(this.circle);
        },
        // the argument i is the iteration number but it doesn't matter
        // because the entire circle was rendered during setup.
        render: function (i) {
        }
    };
}

//
// take the caller's erase definition (the name of an object to erase) and
// return a ItemMachine object with additional information.
//
ItemMachine.prototype.getEraseDef = function(erase) {
    var vector;

    // note that this in the setup and render function is the item being
    // drawn, not the ItemMachine object.
    return {
        erase: erase,
        incr: null,
        iter: 1,
        //
        // setup erases the object with name if it exists
        //
        setup: function() {
            var children = paper.project.activeLayer.children;
            var names = this.erase.name;
            if (!Array.isArray(names)) {
                names = [names];
            }
            names.forEach(function(name) {
                if (children[name]) {
                    children[name].remove();
                }
            });
            /*
            if (paper.project.activeLayer.children[this.erase.name]) {
                paper.project.activeLayer.children[this.erase.name].remove();
            }
            // */
        },
        // the argument i is the iteration number but it doesn't matter
        // because the erase was done at setup.
        render: function (i) {
        }
    };
}


//
// add the lines to ItemMachine's array of line definitions to be drawn
//
ItemMachine.prototype.add = function(items) {
    if (!Array.isArray(items)) {
        items = [items]
    }
    var len = items.length;
    var item;
    var imItem;

    // fastest: https://jsperf.com/array-prototype-push-apply-vs-concat/20
    for (var i = 0; i < len; ++i) {
        item = items[i].item;
        if (item === "line") {
            imItem = this.getLineDef(items[i].def);
        } else if (item === "circle") {
            imItem = this.getCircleDef(items[i].def);
        } else if (item === "erase") {
            imItem = this.getEraseDef(items[i].def);
        } else {
            throw "Invalid item type: " + item;
        }
        imItem.item = item;
        imItem.text = items[i].text;
        imItem.auto = items[i].auto;
        this.items.push(imItem);
    }
}

ItemMachine.prototype.reset = function(opts) {
    this.opts = opts = opts || {};

    this.pause = false;
    this.done = false;
    this.line = null;
    this.t0 = 0;
    this.t1 = 0;
    this.lix = 0;

    paper.view.onFrame = null;
    for (var i = 0; i < this.items.length; i++) {
        if (this.items[i].path) {
            this.items[i].path.remove();
            this.items[i].path = null;
        }
    }
    // skip over the graphpaper group but get any potential leftovers
    for (i = 0; i < paper.project.activeLayer.children.length; i++) {
        if (paper.project.activeLayer.children[i].name !== "graphpaper") {
            paper.project.activeLayer.children[i].remove();
            console.log('found leftover path');
        }
    }

    paper.view.update();
}

//
// Call to have ItemMachine start drawing the lines in it's array of line
// definitions.
//
ItemMachine.prototype.startMachine = function(opts) {
    opts = opts || {};
    this.reset();
    this.mode = opts.mode || "continuous";
    this.q = $.Deferred();

    if (this.items.length <= 0) {
        throw "No lines defined for ItemMachine";
    }

    this.nextItem();

    // return a promise that can receive status updates as well
    // as knowing when all drawing has been completed.
    return this.q;
}

ItemMachine.prototype.nextItem = function() {
    var item = this.items[this.lix];

    this.setupItemForDrawing(item);
    $.when(this.drawItem(item)).then(
        this.itemIsDone()
    );
}

ItemMachine.prototype.nextStep = function() {
    this.nextItem();
}

// toggle or set to specified value
ItemMachine.prototype.pauseMachine = function(pause) {
    if (!pause) {
        this.pause = !this.pause;
    } else {
        this.pause = !!pause;
    }
}

ItemMachine.prototype.switchModes = function(mode) {
    if (mode !== "continuous" && mode !== "single-step") {
        throw "Invalid mode for switchModes: " + mode;
    }
    this.mode = mode;
}

ItemMachine.prototype.setupItemForDrawing = function(item) {
    var path;
    item.setup();
    this.t0 = performance.now();
    return item;
}

ItemMachine.prototype.itemIsDone = function() {
    return function(item) {
        this.t1 = performance.now();
        var status = {
            post: true,
            item: item.item,
            name: item.name,
            text: item.text,
            stats: {
                ms: this.t1 - this.t0,
                iter: item.iter
            }
        }

        if (++this.lix >= this.items.length) {
            this.done = true;
            this.q.resolve(item);
        } else if (this.mode === "continuous") {
            this.q.notify(status);
            this.nextItem();
        } else if (this.mode === "single-step") {
            // don't pause if next item is auto
            if (this.items[this.lix].auto) {
                this.nextItem();
            } else {
                this.q.notify(status);
            }
        } else {
            throw "invalid mode: " + this.mode;
        }
    }.bind(this);
}

//
// execute the item specified by the argument, curitem.
// returns a promise which is resolved when curitem is done.
//
ItemMachine.prototype.drawItem = function(curitem) {
    var q = $.Deferred();
    var i = 0;
    var status = {
        pre: true,
        item: curitem.item,
        name: curitem.name,
        text: curitem.text
    }
    this.q.notify(status);

    paper.view.onFrame = function(e) {
        if (this.pause) {
            return;
        }
        curitem.render(i);

        if (++i >= curitem.iter) {
            paper.view.onFrame = null;
            q.resolve(curitem);
        }
    }.bind(this);

    return q.promise()
}



