
var Controls = (function(c) {
    //
    // constructor for the controls. takes the item machine.
    //
    c = function() {
        this.im;                      // added by initialize()
        this.data = {this: this};     // context for event handlers
        this.btn_start = $("#ctrl-start");
        this.btn_step = $("#ctrl-one-step");
        this.btn_reset = $("#ctrl-reset");
        this.status = $("#status");
        this.startState = "inactive";
        this.stepState = "inactive";
        this.bound_start = this.ctrl_start.bind(this);
        this.bound_step = this.ctrl_step.bind(this);
        this.bound_reset = this.ctrl_reset.bind(this);
        this.setButtons();
        this.enableButtons();
    }

    // constants
    c.startBtnSettings = {
        disabled: {add: "disabled", remove: "", text: "N/A"},
        inactive: {add: "", remove: "disabled", text: "Start"},
        drawing: {add: "", remove: "", text: "Pause"},
        paused: {add: "", remove: "", text: "Resume"},
        done: {add: "", remove: "", text: "Done"}
    }

    c.stepBtnSettings = {
        disabled: {add: "disabled", remove: "", text: "N/A"},
        inactive: {add: "", remove: "disabled", text: "Do First Step"},
        drawing: {add: "", remove: "", text: "Pause"},
        paused: {add: "", remove: "", text: "Resume"},
        waiting: {add: "", remove: "disabled", text: "Do Next Step"},
        done: {add: "", remove: "disabled", text: "Done"}
    }

    // initialize the buttons
    c.prototype.initialize = function(im) {
        this.im = im;
        this.data.im = im;
        this.startState = "inactive";
        this.stepState = "inactive";
        this.setButtons();
    }

    // enable the buttons to handle clicks
    c.prototype.enableButtons = function() {
        this.btn_start.on("click", this.data, this.bound_start);
        this.btn_step.on("click", this.data, this.bound_step);
        this.btn_reset.on("click", this.data, this.bound_reset);
    }

    // set the buttons labels and classes for the mode-states
    c.prototype.setButtons = function() {
        // set text
        this.btn_start.text(c.startBtnSettings[this.startState].text);
        this.btn_step.text(c.stepBtnSettings[this.stepState].text);
        // set classes
        this.btn_start.addClass(c.startBtnSettings[this.startState].add);
        this.btn_start.removeClass(c.startBtnSettings[this.startState].remove);
        this.btn_step.addClass(c.stepBtnSettings[this.stepState].add);
        this.btn_step.removeClass(c.stepBtnSettings[this.stepState].remove);
    }

    // handle the start button state
    c.prototype.ctrl_start = function(e) {
        e.preventDefault();
        // disable step mode when entering continuous
        if (this.startState === "inactive") {
            this.stepState = "disabled";
            var startQ = e.data.im.startMachine({mode: "continuous"});
            this.startState = "drawing";
            $.when(startQ).then(function() {
                this.startState = "done";
                this.setButtons();
            }.bind(this),
            null,
            function(status) {
                if (status.pre && !status.text) {
                    status.text = "";
                }
                this.status.text(status.text);
            }.bind(this));
        } else if (this.startState === "drawing") {
            this.startState = "paused";
            e.data.im.pauseMachine(true);
        } else if (this.startState === "paused") {
            this.startState = "drawing";
            e.data.im.pauseMachine(false);
        } else if (this.startState === "done" || this.startState === "disabled") {
            // do nothing
        } else {
            throw "Invalid startState: " + this.startState;
        }
        this.setButtons();
    }

    // handle the step button state
    c.prototype.ctrl_step = function(e) {
        e.preventDefault();
        if (this.stepState === "inactive") {
            this.startState = "disabled";
            var stepQ = e.data.im.startMachine({mode: "single-step"});
            this.stepState = "drawing";
            $.when(stepQ).then(function() {
                this.stepState = "done";
                this.setButtons();
            }.bind(this),
            null,
            function(status) {
                if (status.pre && !status.text) {
                    status.text = "";
                }
                this.status.text(status.text);
                if (status.post) {
                    this.stepState = "waiting";
                    this.setButtons();
                }
            }.bind(this));
        } else if (this.stepState === "drawing") {
            this.stepState = "paused";
            e.data.im.pauseMachine(true);
        } else if (this.stepState === "paused") {
            this.stepState = "drawing";
            e.data.im.pauseMachine(false);
        } else if (this.stepState === "waiting") {
            this.stepState = "drawing";
            e.data.im.nextStep();
        } else if (this.stepState === "done" || this.stepState === "disabled") {
            // do nothing
        } else {
            throw "Invalid stepState: " + this.stepState;
        }
        this.setButtons();
    }

    // reset the buttons
    c.prototype.ctrl_reset = function(e) {
        e.preventDefault();
        e.data.im.reset();
        this.initialize(this.im);
    }

    // return the constructor function-object
    return c;

})(Controls || {});
