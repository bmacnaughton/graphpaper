//
// definition of graphs. 
//


// shorthand for the polar points
var pp1 = new paper.Point(5, 120);
var pp2 = new paper.Point(5, 330);
var pp3 = new paper.Point(5, 0);
var pp4 = new paper.Point(0, 0);

//
// definition of a polar graph
//
polargraph = {
    // define the type, give it a name
    type: "polar",
    name: "Polar",
    // provide meta information to the specific application. the grapher
    // knows nothing of this.
    meta: {
        title: "Linear Equation - Polar Graph",
        equation: "Not sure yet"
    },
    // define an array of items to be drawn. each item is an object
    items: [{
        // the item is a circle. its definition is an object that will be
        // passed directly to paper.Path.Circle.
        item: "circle",
        def: {
            center: pp1,
            radius: circleRadius,
            fillColor: 'black',
            name: "pt1"
        },
        // this will be passed on status updates
        text: "point at (5, 120)",
    }, {
        item: "circle",
        def: {
            center: pp2,
            radius: circleRadius,
            fillColor: 'black',
            name: "pt2",
        },
        text: "point at (5, 330)",
    }, {
        // the item is a line and def is an object that will be
        // passed as is to paper.Path.Line
        item: "line",
        def: {
            from: pp1,
            to: pp2,
            strokeColor: 'black',
            strokeWidth: strokeWidth,
        },
        text: "draw line from (5, 120) to (5, 330)"
    }, {
        // the item is an erase command. the definition is a name or
        // an array of names defined in previous item(s). these items
        // will be removed from the graph.
        item: "erase",
        def: {
            name: ["pt1", "pt2"],           // can be array or single string
        },
        // auto: true causes this step to be done as part of the previous
        // step even if the graph is being rendered in one-step-at-at-time
        // mode.
        auto: true,
    },
]};

//
// shorthand: easier to define the points here and reuse them below
//
p1 = new paper.Point(-6, 5);
p2 = new paper.Point(6, -3);
p3 = new paper.Point(6, -1);
p4 = new paper.Point(3, -1);
p5 = new paper.Point(9, -3);
p6 = new paper.Point(9, -5);

graph1 = {
    // type of graph
    type: "linear",
    name: "Slope",
    meta: {
        title: "Linear Equations - Simple Method",
        equation: "y = -2/3*x + 1"
    },
    //
    // define the items to be graphed
    //
    items: [{
        item: "circle",
        text: "point at x: -6, y: 5",
        def: {
            center: p1,
            radius: circleRadius,
            fillColor: 'black',
            name: "pt1"
        }
    }, {
        item: "circle",
        text: "point at x: 6, y: -3",
        def: {
            center: p2,
            radius: circleRadius,
            fillColor: 'black',
            name: "pt2",
        }
    }, {
        item: "line",
        text: "draw line from -6, 5 to 6, -3",
        def: {
            from: p1,
            to: p2,
            strokeColor: 'black',
            strokeWidth: strokeWidth,
        },
    }, {
        item: "erase",
        auto: true,                         // advance even in one-step mode
        def: {
            name: ["pt1", "pt2"],           // can be array or single string
        }
    }, {
        item: "line",
        text: "slope is -2/3 so y + 2",
        def: {
            from: p2,
            to: p3,
            style: dashedRed,
        }
    }, {
        item: "line",
        text: "slope is -2/3 so raise y + 2 then x - 3", 
        def: {
            from: p3,
            to: p4,
            style: dashedRed
        }
    }, {
        item: "line",
        text: "or x + 3",
        def: {
            from: p2,
            to: p5,
            style: dashedRed,
        }
    }, {
        item: "line",
        text: "then y - 2",
        def: {
            from: p5,
            to: p6,
            style: dashedRed,
        }
    }, {
        item: "circle",
        text: "to find the next point at 9, -5",
        def: {
            center: p6,
            radius: circleRadius,
            fillColor: 'black'
        }
    }, {
        item: "line",
        text: "and continue the line",
        def: {
            from: p4,
            to: p6,
            strokeColor: 'black',
            strokeWidth: strokeWidth,
        }
    }
]};

// this is specific to this implementation, not graphpaper in general.
// set graph equal to one of the graphs: graph1 or polargraph.
graphs = [graph1, polargraph];
