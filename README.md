# README #

1. Copy the files to a directory of your choosing.
2. Create a subdirectory named vendor/
3. Download jquery-2.1.3.js and place it in the vendor/ directory.
4. Download paper-0.9.25.js and place it in the vendor/ directory (this will need to be from paper-core.js).
5. Alternatively, reference jquery and paper from a CDN.

Load template.html in your favorite browser. I have tested it in IE 11, Chrome, and Firefox.

### What is this repository for? ###

* Animate graphs with text notes for school presentations.
* Version 1.0

### How do I use it? ###
Load template.html in your browser. There are a few files that illustrate how it's used:

1. controls.js - this handles clicks on the buttons on the bottom of the screen and changes display text based on status messages associated with the promise returned by the graphing engine.
2. graph1.js - this has the graph definitions in it and has reasonable documentation as comments.
3. config.js - this defines a few globals. it needs to be reworked as it mixes globals used by template.html with those used by graph1.js. it was set up so my daughter only had to edit one file if the screen at school needed different parameters.

### What's missing? ###

* Tests
* Documentation
* a CSS file - it's all inline

It's version 1.0 of a tool for my daughter's math project.

### Contribution guidelines ###

* Use 4 space indentations and no tabs
* Try to keep to the style I use

### Who do I talk to? ###

* bmacnaughton