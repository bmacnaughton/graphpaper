//
// global definitions for this particular use of graphpaper
//
graphSize = 500;      // total size of graph in pixels
graphRange = 10;      // minimum points on positive/negative x and y axes

//
// definitions for the graphs
//
circleRadius = graphSize/200;
strokeWidth = graphSize/150;

dashedRed = {
    strokeColor: 'red',
    dashArray: [3, 3],
    strokeWidth: strokeWidth
};

